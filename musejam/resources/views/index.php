<!doctype html>
<html>
    <head>
        <title> Musejam | Destination Routes</title>
        <link href="<?php echo asset('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('assets/css/style.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset('assets/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />

        <script src="<?php echo asset('assets/js/jquery.min.js') ?>"></script>
        <script src="<?php echo asset('assets/js/bootstrap.min.js') ?>"></script>

        <script src="<?php echo asset('app/lib/angular.js') ?>"></script>
        <script src="<?php echo asset('app/app.js') ?>"></script>
        <script src="<?php echo asset('app/controllers/destinations.js') ?>"></script>
    </head>
    <body ng-app="destinationRoutes">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href=""><i class="fa fa-home "> </i> Musejam</a>
                </div>
                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="javascript:void(0)">Created By - Sumit Kohli</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container" ng-controller="destinationsController">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-subway"></i> Destinations Routes</h3>                    

                    <div class="alert alert-success" ng-hide="!search_routes.paths.length || error">
                        We have found <b>{{search_routes.total_routes}}</b> station(s) from <b>Station {{search_routes.from}}</b> to <b>Station {{search_routes.destination}}</b>
                    </div>
                </div>
            </div>
            <!-- /.row -->

            <!-- Content Row -->
            <div class="row">
                <div class="col-md-3">
                    <div class="list-group">
                        <form role="form" ng-submit="fetch_results()">
                            <div class="form-group">
                                <label>From</label>
                                <select class="form-control" ng-model="searchForm.from">
                                    <option ng-repeat="dest in destinations" value="{{dest.id}}"> Station {{ dest.station_name}}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Destination</label>
                                <select class="form-control" ng-model="searchForm.destination">
                                    <option ng-repeat="dest in destinations" value="{{dest.id}}"> Station {{ dest.station_name}}</option>
                                </select>
                            </div>
                            <button class="btn btn-primary" type="submit">Search</button>
                        </form>
                    </div>
                </div>
                <!-- Content Column -->
                <div class="col-md-9">
                    <div ng-show="error" class="panel panel-danger">
                        <div class="panel-heading">
                            <i class="fa fa-thumbs-o-down"></i> Sorry
                        </div>
                        <div class="panel-body">
                            <p>{{error_message}}</p>
                        </div>
                    </div>
                    
                    <div ng-show="!search_routes.paths.length && !error" class="panel panel-primary">
                        <div class="panel-heading">
                            Welcome to Musejam Metro
                        </div>
                        <div class="panel-body">
                            <p>Search to find the routes between the stations</p>
                        </div>
                    </div>

                    <div class="row" ng-repeat="route in search_routes.paths">

                        <div class="col-lg-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">Total Distance : {{route.distance}}</div>
                                <div class="panel-body">
                                    
                                    <span ng-repeat="station in route.nodes">
                                        <b>{{getSperator(station.distance)}}</b>
                                        <span class="stations">{{station.station}}</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .stations {background-color: darkblue; border-radius: 50%; color: white; padding: 5px 9px 4px 8px;}
        </style>
    </body>
</html>
