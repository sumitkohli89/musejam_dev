<!doctype html>
<html>
    <head>
        <title> {!! $page_title or 'Sumit\'s Creation' !!} </title>
        <link href="{!! asset('assets/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('assets/css/style.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('assets/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css" />

        <script src="{!! asset('assets/js/jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>

        @stack('scripts')

    </head>
    <body>
        @include('common.navigation')

        <div class="container">
            @include($page_view)
        </div>
    </body>
</html>
