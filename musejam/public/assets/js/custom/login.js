$(document).on('click', '.login-button', function () {
    var current_element = $(this);
    current_element.html('Loading').removeClass('login-button').addClass('disabled');
    $('.error').html('');

    $.post('login', current_element.closest('form').serialize(), function (e) {
        if (e.status == true) {
            window.location.href = current_element.data('redirect_uri') + '&authorization_code=' + e.auth_id;
        } else {
            $('.error').append('<div class="alert alert-danger">' + e.message + '</div>');
        }

        current_element.addClass('login-button').removeClass('disabled').html('Login');
    }, 'json')
    .error(function(e) { 
        var errors = $.parseJSON(e.responseText);

        $.each(errors, function (index, value) {
            $('.error').append('<div class="alert alert-danger">' + value + '</div>');
        });
        current_element.addClass('login-button').removeClass('disabled').html('Login');
    });

    return false;
});
   