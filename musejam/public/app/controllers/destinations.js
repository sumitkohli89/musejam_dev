app.controller('destinationsController', function ($scope, $http, API_URL) {

    $http.get(API_URL + "routes/getAllStations")
        .success(function(response) {
            $scope.destinations = response;
        });


    //Fetch Destinations
    $scope.search_routes = [];
    $scope.error = false;
    $scope.error_message = '';
    $scope.searchForm = {from: "1", destination: "6"};

    $scope.number = 0;
    $scope.getSperator = function (num) {
        var input = '';

        for (var i = 0; i < num * 4; i++) {
            
            if (i == (num * 4) / 2) {
//                input = input + '<sup>' + num + '</sup>';
                //input = input + num;
            } else {
                input = input + ' - ';
            }

        }
        return input;
    }

    $scope.fetch_results = function () {
        var url = API_URL + "routes/search";

        $http({
            method: 'POST',
            url: url,
            data: $.param($scope.searchForm),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            if (response.status == true) {
                $scope.error = false;
                $scope.search_routes = response.response;
            } else {
                $scope.search_routes = [];
                $scope.error = true;
                $scope.error_message = response.response.error;
            } 
        }).error(function (response) {
            
        });
    }

});
