<?php

namespace App\Libraries\Bot;

abstract class SocialOutput {

    private static $instance = NULL;

    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    abstract function getResponseParams($param);
}
