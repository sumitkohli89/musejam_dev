<?php

namespace App\Libraries\Bot;

abstract class SocialInput {

    private static $instance = NULL;

    public static function getInstance() {
        if (null === self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    abstract function getRequestParams($param);
}
