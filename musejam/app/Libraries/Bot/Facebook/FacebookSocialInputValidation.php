<?php

namespace App\Libraries\Bot\Facebook;

use Illuminate\Support\Facades\Validator;

class FacebookSocialInputValidation extends \App\Libraries\Bot\SocialInputValidation {

    public function validate($data, $params) {
        if (empty($params['validator'])) { return ['status' => TRUE]; }

        if ('fixed' == $params['validator']['type']) {
            $v = Validator::make(['data' => $data['input']], [
               'data' => $params['validator']['rule'],
            ]);

            if ($v->fails()) {
                $data['action'] = config('constants.FACEBOOK.DEFAULT_EVENTS.text');
                $data['output'] = $params['validator']['message'];
 
                return ['status' => FALSE, 'response' => $data];
            }
        } elseif ('custom' == $params['validator']['type']) {
            return $this->$params['validator']['name']();
        }

        return ['status' => TRUE];
    }
}
