<?php

namespace App\Libraries\Bot\Facebook;

class FacebookSocialInput extends \App\Libraries\Bot\SocialInput {

    /* 
     * Description - To check if a request is text
     */
    public function is_request_text($request_param) {
        if (empty($request_param['message']['text']) || !empty($request_param['message']['is_echo'])) return ['status' => FALSE];

        return [
            'status' => TRUE,
            'params' => [
                'message_id' => $request_param['message']['mid'],
                'timestamp' => $request_param['timestamp'],
                'type' => 'text',
                'user_id' => $request_param['sender']['id'],
                'message_sequence' => $request_param['message']['seq'],
                'input' => $request_param['message']['text']
            ]
        ];
    }

    /* 
     * Description - To check if a request is payload
     */
    public function is_request_payload($request_param) {
        if (empty($request_param['postback']['payload'])) return ['status' => FALSE];

        return [
            'status' => TRUE,
            'params' => [
                'timestamp' => $request_param['timestamp'],
                'type' => 'payload',
                'user_id' => $request_param['sender']['id'],
                'input' => $request_param['postback']['payload']
            ]
        ];
    }

    /* 
     * Description - To check if a request is payload
     */
    public function is_request_auth($request_param) {
        if (empty($request_param['account_linking']['status'])) return ['status' => FALSE];

        $return = [
            'status' => TRUE,
            'params' => [
                'timestamp' => $request_param['timestamp'],
                'user_id' => $request_param['sender']['id']
            ]
        ];

        /* Logout Success */
        if ($request_param['account_linking']['status'] == 'unlinked') {
            $return['params']['type'] = 'logout_success';
            return $return;
        }

        /* Login Success */
        if ($request_param['account_linking']['status'] == 'linked') {
            $return['params']['type'] = 'login_success';
            $return['params']['input'] = $request_param['account_linking']['authorization_code'];
            return $return;
        }

        /* Login Failed */
        $return['params']['type'] = 'login_failed';
        
        return $return;
    }
    
    /* Extending method to return request parameters */
    public function getRequestParams($request) {

        $master_node = $request->all()['entry'][0]['messaging'][0];

        /* Checking of request is text type */
        $is_text = $this->is_request_text($master_node);
        if (TRUE === $is_text['status']) {return $is_text['params'];}

        /* Checking of request is text type */
        $is_payload = $this->is_request_payload($master_node);
        if (TRUE === $is_payload['status']) {return $is_payload['params'];}

        /* Checking of request is text type */
        $is_login_success = $this->is_request_auth($master_node);
        if (TRUE === $is_login_success['status']) {return $is_login_success['params'];}
        
        return FALSE;
    }   
}
