<?php

namespace App\Libraries\Bot\Facebook;

class FacebookSocialOutput extends \App\Libraries\Bot\SocialOutput {

    private function no_thread_exist($requestParams, $getKeywordDetails) {

        /*If no keyword exist in db*/
        if (empty($getKeywordDetails)) {
            \App\Libraries\ExceptionHandler::throwException('NO_KEYWORD_FOUND'); 
        }

        switch ($getKeywordDetails->action_type) {

            case 'text' :
                $requestParams['action'] = 'post_text';
                $requestParams['output'] = $getKeywordDetails->rules;
                break;

            case 'payload' :
                $requestParams['action'] = $getKeywordDetails->rules;
                $requestParams['action_id'] = $getKeywordDetails->action_id;
                break;

            case 'thread' :
                /* Creating Thread */
                $thread = \App\Models\Thread::save_thread([
                    'social_id' => $requestParams['user_id'],
                    'action_id' => $getKeywordDetails->action_id,
                    'status' => config('constants.DB_STATUS.PENDING'),
                    'request' => $getKeywordDetails->rules,
                ]);

                /* runing first Rule */
                $rules = json_decode($getKeywordDetails->rules, TRUE);
                $first_rule = current($rules);
                $requestParams = array_merge($requestParams, $first_rule);
                $requestParams['params']['thread_id'] = $thread->id; 
                break;            
        }
        return $requestParams;
    }

    private function thread_exist($requestParams, $thread_details) {
        $response = !empty($thread_details->response) ? json_decode($thread_details->response, TRUE) : [];
        $is_save = TRUE;

        $request_params = json_decode($thread_details->request, TRUE);
        $current_thread = current($request_params);
        $response[$current_thread['key_name']] = $requestParams['input'];
        $is_save = isset($current_thread['is_save']) && is_null($current_thread['is_save']) == FALSE ? $current_thread['is_save'] : $is_save; 
        array_shift($request_params);

        /* Validation of thread */
        $validation = FacebookSocialInputValidation::getInstance()->validate($requestParams, $current_thread);

        if (FALSE === $validation['status']) return $validation['response'];        

        /* Sending Post */
        $current_thread = current($request_params);

        /*Check if dependent on Previous reply*/
        if (!empty($current_thread['dependent']) && TRUE == $current_thread['dependent']) {
            $current_thread = array_merge($current_thread, $current_thread[$requestParams['input']]);
        }

        $requestParams['params'] = $response;
        $requestParams['params']['thread_id'] = $thread_details->id;
        $requestParams = array_merge($requestParams, $current_thread);

        /* Saving */
        $save_data = ['request' => json_encode($request_params), 'response' => json_encode($response)];

        if ($current_thread['key_name'] == last($request_params)['key_name']) {
            $save_data ['status'] = config('constants.DB_STATUS.ACTIVE');
        }
        if (TRUE === $is_save) {
            \App\Models\Thread::save_thread($save_data, ['id' => $thread_details->id]);
        } else {
            $requestParams['save_data'] = $save_data;
        }

        return $requestParams;
    }


    /* 
     * Extending method to return response parameters 
     */
    public function getResponseParams($requestParams) {
 
        switch ($requestParams['type']) {
            case 'login_success' :
            case 'logout_success' :
                $requestParams['action'] = config('constants.FACEBOOK.DEFAULT_EVENTS.' . $requestParams['type']);
                return $requestParams;
                break;

            default :
                $getKeywordDetails = \App\Models\Keyword::getKeywordDetails($requestParams['input']);
                
                if (!empty($getKeywordDetails) && $getKeywordDetails->is_wild == TRUE) {
                    return $this->no_thread_exist($requestParams, $getKeywordDetails);
                }

                /* Check if thread is already initiated */
                $thread_details = \App\Models\Thread::getThreadDetails($requestParams['user_id']);

                return empty($thread_details) ? $this->no_thread_exist($requestParams, $getKeywordDetails) : 
                    $this->thread_exist($requestParams, $thread_details);
                break;
        }
    }

}
