<?php

namespace App\Libraries;

class Util {

    public static function p($data, $die = TRUE) {
        echo '<pre>';
        print_r($data);

        if ($die) {
            die;
        }
    }

    public static function post_data($url, $data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        $response = curl_exec($ch);
        $err = curl_error($ch);

        if ($err) {
            return json_decode($err) ? json_decode($err, TRUE) : ['error' => ['message' => $err]];
        }
        curl_close($ch);
        return json_decode($response) ? json_decode($response, TRUE) : $response;
    }

    public static function prepare_mdn($mobile) {
        if (empty($mobile))
            return FALSE;

        if (strlen($mobile) == 10) {
            return '91' . $mobile;
        }
        return $mobile;
    }

    public static function get_data($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);

        if ($err) {
            return json_decode($err) ? json_decode($err, TRUE) : ['error' => ['message' => $err]];
        }
        return json_decode($output) ? json_decode($output, TRUE) : $output;
    }
    
    public static function convert_rupee_to_paise($amount) {
        return (string) ( round(100 * $amount));
    }

}
