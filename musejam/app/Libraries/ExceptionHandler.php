<?php

namespace App\Libraries;

class ExceptionHandler {
    
    /*
     * Function to throw message
     */
    public static function throwException($key) {
        $error_details = config('errormessages.SYSTEM_MESSAGES.' . $key);

        throw new \Exception($error_details['message'], $error_details['code']);
    }

    public static function handle($request, $error) {
        $is_code_defined = self::isCodeDefined($error);

        if (TRUE === $is_code_defined['status'] && TRUE === $is_code_defined['details']['is_send']) {
            return self::post_error($request, $is_code_defined['details']);
        }
        return self::defaultError($request, $error);
    }

    /* If code is defined in the system */
    public static function isCodeDefined($error) {
        $error_messgaes = config('errormessages');

        if (empty($error_messgaes['SYSTEM_MESSAGES_REVERSE_MAPPING'][$error->getCode()])) {
            return ['status' => FALSE];
        }
        $error_details = $error_messgaes['SYSTEM_MESSAGES'][$error_messgaes['SYSTEM_MESSAGES_REVERSE_MAPPING'][$error->getCode()]];
        
        return [
            'status' => TRUE,
            'details' => $error_details
        ];
    }

    public static function post_error($request, $error) {
        $request->merge(['request_params' => [
            'action' => 'post_text',
            'output' => $error['message'],
            'user_id' => $request['entry'][0]['messaging'][0]['sender']['id']
        ]]);
        return $request;
    }

    public static function defaultError($request, $error) {
        Util::p($error->getMessage());
        \App\Models\Log::save_logs(['request' => json_encode($error)]); 
        die;
    }
    
    /*
     * Description - Error Response for facebook
     * All the logs should be applied here
     * @author - Sumit Kohli
     */
    public static function throwFbFailure($error) {
        $fb_messages = config('errormessages.FACEBOOK_ERRORS');

        if (!empty($error['code']) && !empty($fb_messages[$error['code']])) {
            self::throwException($fb_messages[$error['code']]);
        }
        self::throwException('CURL_ERROR');
    }

    /*
     * Description - Error Response for facebook
     * All the logs should be applied here
     * @author - Sumit Kohli
     */
    public static function throwOxifaceFailure($request) {
        $oxiface_messages = config('errormessages.OXIFACE_ERRORS');

        if (!empty($request['service_response']['response_info']['host_code']) && 
                !empty($oxiface_messages[$request['service_response']['response_info']['host_code']])) {

            self::throwException($oxiface_messages[$request['service_response']['response_info']['host_code']]);
        }
        self::throwException('CURL_ERROR');
    }

}
