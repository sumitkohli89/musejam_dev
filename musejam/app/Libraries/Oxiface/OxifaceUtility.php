<?php

namespace App\Libraries\Oxiface;

class OxifaceUtility {

    public static function generateRandomNumber($length = 6) {
        $vCode = NULL;
        $possible = "01234567890123456789";
        $i = 0;

        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);
            $vCode .= $char;
            $i++;
        }
        return $vCode;
    }

    public static function check_api_success($response) {
        if (empty($response)) {
            return FALSE;
        }
        if (isset($response['service_response']['response_info']['host_code']) &&
                $response['service_response']['response_info']['host_code'] == 0
        ) {
            return TRUE;
        }
        return FALSE;
    }

    public static function prepareRequest($service, $content = []) {
        $requestArr = [
            '_service' => $service,
            '_version' => "1.0",
            'service_request' => [
                'channel_info' => [
                    'requester' => config('constants.OXIFACE.REQUESTER'),
                    'instrument' => config('constants.OXIFACE.INSTRUMENT'),
                    'channel_instance_id' => '01'
                ],
                'device_info' => [
                    'device_os' => 'WINDOWS',
                    'device_os_version' => '4.4.0',
                    'device_id' => "87612736878417868"
                ],
                'transaction_info' => [
                    'time_stamp' => date('c'),
                    'request_id' => self::generateRandomNumber(20),
                    'txn_description' => $service
                ],
                'params' => [
                    'param_1' => "",
                    'param_2' => "",
                    'param_3' => "",
                    'param_4' => "",
                    'param_5' => ""
                ]
            ]
        ];

        if (!empty($content)) {
            $requestArr['service_request'] = array_merge($requestArr['service_request'], $content);
        }
        return $requestArr;
    }
}
