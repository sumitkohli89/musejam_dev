<?php

namespace App\Libraries\Oxiface;
use App\Libraries\Oxiface\OxifaceUtility;

class OxifaceApiAdapter {
    
    private static $instance = null;

    public static function instance() {
        if (null === self::$instance) {
            self::$instance = new static();
        }
        return self::$instance;
    }
    
    /*
     * Oxiface - Wallet Info API
     */
    public function wallet_info ($user_data = []) {
        $user_data['mdn'] = \Util::prepare_mdn($user_data['mdn']);

        $request = OxifaceUtility::prepareRequest('WALLET_INFO', [
            'user' => ['mdn' => $user_data['mdn']]
        ]);
        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }

        \App\Libraries\ExceptionHandler::throwException('HOST_ERROR');
    }

    /*
     * Oxiface - User Verify Api
     */
    public function user_verify ($user_data = []) {
        $user_data['mdn'] = \Util::prepare_mdn($user_data['mdn']);
        $request = OxifaceUtility::prepareRequest('USER_VERIFY', [
            'user' => ['mdn' => $user_data['mdn'], 'is_loggedin' => '0'], 
            'password_info' => ['type' => 'VERIFY_USER', 'security_type' => 'TYPE_0', 'data' => $user_data['password']]
        ]);
        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }

        return ['status' => FALSE, 'response' => $response];
    }
    
    /*
     * User Info of Oxigen User
     */
    public function user_info ($mdn) {
        $mdn = \Util::prepare_mdn($mdn);

        $request = OxifaceUtility::prepareRequest('USER_INFO', [
            'user' => ['mdn' => $mdn]
        ]);
        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }

        return ['status' => FALSE, 'response' => $response];
    }

    /*
     * User Info of Oxigen User
     */
    public function vas_wallet_to_wallet ($mdn, $details) {

        $request = OxifaceUtility::prepareRequest('VAS_WALLETTOWALLET', [
            'user' => ['mdn' => \Util::prepare_mdn($mdn)],
            'transaction_data' => [
                'amount' => \Util::convert_rupee_to_paise($details['amount']),
                'currency_code' => 'INR',
                'beneficiary_mdn' => \Util::prepare_mdn($details['beneficiary_mdn']),
                'is_social' => TRUE
            ]
        ]);

        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }

        return ['status' => FALSE, 'response' => $response];
    }
    /*
     * Vas
     */
    public function vas ($mdn, $details) {

        $request = OxifaceUtility::prepareRequest('VAS', [
            'user' => ['mdn' => \Util::prepare_mdn($mdn)],
            'transaction_data' => [
                'amount' => \Util::convert_rupee_to_paise($details['amount']),
                'currency_code' => 'INR',
                'vas_type' => $details['vas_type'],
                'service' => $details['service'],
                'mode' => 'WALLET',
                'is_social' => TRUE,
                'operator' => $details['operator']
            ],
            'service_entities' => [
                ['type' => 'service_number', 'value' => \Util::prepare_mdn($details['beneficiary_mdn'])]
            ]
        ]);

        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }

        return ['status' => FALSE, 'response' => $response];
    }
    

    /*
     * Create Otp
     */
    public function user_createotp ($mdn, $details) {

        $request = OxifaceUtility::prepareRequest('USER_CREATEOTP', [
            'user' => ['mdn' => \Util::prepare_mdn($mdn), 'is_loggedin' => '0'],
            'otp_info' => [
                'type' => $details['otp_type']
            ]
        ]);

        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }
        return ['status' => FALSE, 'response' => $response];
    }

    /*
     * Verify Otp
     */
    public function user_verifyotp ($mdn, $details) {

        $request = OxifaceUtility::prepareRequest('USER_VERIFYOTP', [
            'user' => ['mdn' => \Util::prepare_mdn($mdn), 'is_loggedin' => '0'],
            'otp_info' => [
                'type' => $details['otp_type'],
                'security_type' => 'TYPE_0',
                'data' => $details['otp']
            ]
        ]);

        $response = \Util::post_data(config('constants.OXIFACE.API_URL'), $request);

        if (OxifaceUtility::check_api_success($response)) {
            return ['status' => TRUE, 'response' => $response];
        }
        return ['status' => FALSE, 'response' => $response];
    }
}
