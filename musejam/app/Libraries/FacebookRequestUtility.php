<?php

namespace App\Libraries;

class FacebookRequestUtility {

    /*
     * Description - This function is used to post data to facebook
     * All the logs should be applied here
     * @author - Sumit Kohli
     */
    public static function postToFacebook($url, $data) {
        $response = Util::post_data($url, $data);

        /* Error Handling */
        if (!empty($response['error'])) {
            /* Saving Logs in case of error */
            \App\Models\Log::save_logs(['request' => json_encode($data), 'response' => json_encode($response)]);

            return ExceptionHandler::throwFbFailure($response['error']);
        }

        return $response;
    }

    /*
     * Description - This function is to hit get request to facebook
     * All the logs should be applied here
     * @author - Sumit Kohli
     */
    public static function getToFacebook($url) {
        $response = Util::get_data($url);

        /* Error Handling */
        if (!empty($response['error'])) {
            /* Saving Logs in case of error */
            \App\Models\Log::save_logs(['request' => json_encode($url), 'response' => json_encode($response)]);

            return ExceptionHandler::throwFbFailure($response['error']);
        }

        return $response;
    }
    
    /*
     * Text Template - Post text to facebook
     * @author - Sumit Kohli
     */
    public static function postText($req) {
        $build_data = [
            'recipient' => ['id' => $req['user_id']],
            'message' => ['text' => $req['text']]
        ];

        /* Need to implement error handling */
        self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

    /*
     * Action Template - Post Actions to reciever
     * @author - Sumit Kohli
     */
    public static function senderAction($req) {
        $build_data = [
            'recipient' => ['id' => $req['user_id']],
            'sender_action' => $req['sender_action']
        ];

        /* Need to implement error handling */
        self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }
    
    /*
     * Action Template - Build Button Template
     * @author - Sumit Kohli
     */
    public static function buildButtonTemplate($req) {
        $build_data = [];
        $build_data['recipient'] = ['id' => $req['user_id']];
        $build_data['message'] = [
            'attachment' => ['type' => 'template', 'payload' => [
                'template_type' => 'button',
                'text' => $req['title'],
                'buttons' => $req['buttons']
            ]
        ]];
        return $build_data;
    }

    /*
     * Action Template - Build Generic template
     * @author - Sumit Kohli
     */
    public static function buildGenericTemplate($req) {        
        $build_data = $elements = [];
        $build_data['recipient'] = ['id' => $req['user_id']];
        $build_data['message'] = [
            'attachment' => [
                'type' => 'template', 
                'payload' => [
                    'template_type' => 'generic'
                ]
            ]
        ];

        if (!empty($req['elements'])) {
            $non_required_keys = ['item_url', 'image_url', 'subtitle', 'buttons'];

            foreach ($req['elements'] as $val) {
                $element = ['title' => $val['title']];

                foreach ($non_required_keys as $value) {
                    if (empty($val[$value])) continue;

                    $element[$value] = $val[$value];
                }
                $elements[] = $element;
            }
            $build_data['message']['attachment']['payload']['elements'] = $elements;
        }
        
        return $build_data;
    }

    /*
     * Action Template - Set Start button pay;oad
     * @author - Sumit Kohli
     */
    public static function setStartButton ($req) {
        $build_data = [
            'setting_type' => 'call_to_actions',
            'thread_state' => 'new_thread',
            'call_to_actions' => [
                ['payload' => $req['payload']]
            ],
        ];
        return $build_data;
    }

    /*
     * Action Template - Set Start button pay;oad
     * @author - Sumit Kohli
     */
    public static function setGreetingText ($req) {
        $build_data = [
            'setting_type' => 'greeting',
            'greeting' => [
                'text' => $req['text']
            ],
        ];
        return $build_data;
    }

    /*
     * Action Template - Set Start button pay;oad
     * @author - Sumit Kohli
     */
    public static function buildQuickReplies ($req) {
        $build_data = [
            'recipient' => ['id' => $req['user_id']],
            'message' => [
                'title' => $req['title']
            ]
        ];

        foreach ($req['quick_replies'] as $val) {
            $build_data['message']['quick_replies'] = [
                'content_type' => 'text',
                'title' => $val['title'],
                'payload' => $val['payload']
            ];
        }
        return $build_data;
    }
    

 /* *************************Templating Ends***********************************/


    
    /*
     * Show services
     */

    public static function show_services($req) {
        $req['title'] = 'How may I help you ?';
        $req['buttons'] = [
            [
                'type' => 'postback',
                'title' => 'Mobile Recharge',
                'payload' => 'mobile_recharge',
            ],
            [
                'type' => 'postback',
                'title' => 'Get balance',
                'payload' => 'my_balance',
            ],
            [
                'type' => 'postback',
                'title' => 'Send money to friend',
                'payload' => 'p2p',
            ]
        ];
        
        $build_data = self::buildButtonTemplate($req);
       
        self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

    /* Login Template */
    public static function login_template($req) {
        $req['elements'] = [
            [
                'title' => 'Welcome to oxigen wallet',
                'image_url' => 'https://gorakhpuriya.com/oxibot/public/assets/images/oxigen.png',
                'subtitle' => 'Kindly login to use services',
                'buttons' => [
                    ['type' => 'account_link', 'url' => "https://gorakhpuriya.com/oxibot/public/bot/login"]
                ]
            ]
        ];

        $build_data = self::buildGenericTemplate($req);

        return self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }
    
    
    /* Logout Template */
    public static function logout_template($req) {
        $req['elements'] = [
            [
                'title' => 'Come back soon.',
                'image_url' => 'https://gorakhpuriya.com/oxibot/public/assets/images/oxigen.png',
                'buttons' => [
                    ['type' => 'account_unlink']
                ]
            ]
        ];

        $build_data = self::buildGenericTemplate($req);

        return self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

    /* Login Template */

    public static function get_user_id($req) {
        return self::getToFacebook(config('constants.FACEBOOK.API_URL') . 'me?fields=recipient&access_token=' . config('constants.FACEBOOK.PAGE_TOKEN') . '&account_linking_token=' . $req['account_linking_token']);
    }

    /* Setup Start Button - One time */

    public static function start_button() {
        $build_data = self::setStartButton(['payload' => 'start_button_success']);
        return self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/thread_settings?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

    
    /* Setup Greeting Text - One time */
    public static function greeting_text() {
        $build_data = self::setGreetingText([
            'text' => config('constants.SITE_MESSAGES.GREETING_TEXT')
        ]);

        return self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/thread_settings?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

    /*
     * Show services
     */

    public static function get_operators($req) {
        /*Preparing Elements*/
        $hold_data = []; $i = 1;

        foreach (config('constants.VAS_OPERATORS.ETOP.operators') as $alias => $title) {
            $hold_data[] = ['type' => 'postback', 'title' => $title, 'payload' => $alias];

            if ((($i % 3) == 0 && $i != 1) || $i == count(config('constants.VAS_OPERATORS.ETOP.operators'))) {
                $req['elements'][] = ['title' => 'Select operator', 'buttons' => $hold_data];
                $hold_data = [];
            }
            $i++;
        }

        $build_data = self::buildGenericTemplate($req);
        return self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

    /*
     * Show services
     */
    public static function confirm_vas_template($req) {
        /*Preparing Elements*/
        $req['title'] = '₹' . $req['amount'] . ', ' . $req['beneficiary_mdn'] . ', ' . $req['operator'];
        $req['buttons'] = [
            [
                'type' => 'postback',
                'title' => 'Pay via wallet',
                'payload' => config('constants.EVENTS.VAS_RECHARGE.action'),
            ],
            [
                'type' => 'postback',
                'title' => 'Cancel Payment',
                'payload' => config('constants.EVENTS.CANCEL_TRANSACTION.action')
            ]
        ];

        $build_data = self::buildButtonTemplate($req);
        return self::postToFacebook(config('constants.FACEBOOK.API_URL') . 'me/messages?access_token=' . config('constants.FACEBOOK.PAGE_TOKEN'), $build_data);
    }

}
