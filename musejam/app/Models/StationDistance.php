<?php

namespace App\Models;

class StationDistance extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'station_distance';
    
    public static function get_all($conditions = [], $params = []) {
        $builder = self::where($conditions);

        if (!empty($params['fields'])) { $builder->select($params['fields']); }
        if (!empty($params['first'])) { return $builder->first(); }

        return $builder->get()->toArray();
    }
}
