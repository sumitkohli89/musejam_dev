<?php

namespace App\Models;

class Station extends \Illuminate\Database\Eloquent\Model {

    
    public static function get_all($conditions = [], $params = []) {
        $builder = self::where($conditions);

        if (!empty($params['fields'])) { $builder->select($params['fields']); }
        if (!empty($params['first'])) { return $builder->first(); }

        return $builder->get()->toArray();
    }
    
    public static function getByUniqueKey($unique_key) {
        if (empty($unique_key)) { return FALSE; }

        try {
            return self::get_all(
                ['unique_key' => $unique_key, ],
                ['first' => TRUE, 'fields' => ['id', 'rules']]
            );
        } catch (\Illuminate\Database\QueryException $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
