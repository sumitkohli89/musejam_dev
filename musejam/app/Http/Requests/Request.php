<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

    public function messages() {
        return [
            'email' => 'Enter valid :attribute',
            'required' => ':attribute is required',
        ];
    }
}
