<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
Route::get('/', function () {
    return view('index');
});

Route::post('api/routes/search', ['as' => 'Routes', 'uses' => 'Api\RoutesController@search']);
Route::get('api/routes/getAllStations', ['as' => 'RoutesStations', 'uses' => 'Api\RoutesController@getAllStations']);

