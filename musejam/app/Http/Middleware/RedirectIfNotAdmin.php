<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin') {
        if (Auth::guard($guard)->check() == FALSE) {
            return redirect('/admin/login');
        } else if ($this->has_permission($request) == FALSE) {
            return redirect('/admin/unauthorised');
        }
        return $next($request);
    }

    public function has_permission($request) {
        $route_details = $request->route()->getAction();
        $user_roles = \Illuminate\Support\Facades\Session::get('roles');
        $currentRoute = explode('\\', $route_details['controller']);

        return !empty($user_roles[end($currentRoute)]) ? TRUE : FALSE;
    }
    
    

}
