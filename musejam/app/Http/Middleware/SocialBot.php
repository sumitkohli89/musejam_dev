<?php

namespace App\Http\Middleware;

use Closure;

class SocialBot {

    /**
     * Handle an incoming request.
     * @author - Sumit Kohli
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try {
            /* Logging Raw Request */
            \App\Models\Log::save_logs(['request' => json_encode(['headers' => getallheaders(), 'request' => $request->all()])]);
            $request_params = $this->getRequestParams($request);
            
            if (!empty($request_params)) {
                $request->merge(['request_params' => $request_params]);            
                return $next($request);
            }
        } catch (\Exception $e) {
            $exception = \App\Libraries\ExceptionHandler::handle($request, $e);
            return $next($exception);
        }
    }

    /*
     * Give the source of request - Facebook, twitter etc 
     * @param $request - object
     * @return string Name of the source 
     * @author - Sumit Kohli
     */
    private function getRequestSource($request) {
        return $request->route()->getName();
    }

    /*
     * Fetch necassary inputs from Request
     * @param $request - array of request params
     * @return array
     * @author - Sumit Kohli
     */
    private function getRequestParams($request) {
        $requester = $this->getRequestSource($request);
        $utilityInput = '\\App\Libraries\\Bot\\' . $requester . '\\' . $requester . 'SocialInput';
        $requestParameters = $utilityInput::getInstance()->getRequestParams($request);

        if (!empty($requestParameters)) {
            $utilityOutput = '\\App\Libraries\\Bot\\' . $requester . '\\' . $requester . 'SocialOutput';
            return $utilityOutput::getInstance()->getResponseParams($requestParameters);
        }
    }
    
    /*
     * Fetch necassary inputs from Request
     * @param $request - array of request params
     * @return array
     * @author - Sumit Kohli
     */
    private function verifyWebhook() {
        if (isset($_GET['hub_verify_token'])) {
            if ($_GET['hub_verify_token'] === 'mamashree_ki_jai_ho') {
                echo $_GET['hub_challenge'];
                die;
            } else {
                echo 'Invalid Verify Token';
                die;
            }
        }
        die;
    }
    
    

}
