<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoutesController extends Controller {

    /**
     * @author Sumit Kohli <sumit.kohli12345@gmail.com>
     * Description - Display a listing of All stations.
     *
     * @return JSON Response
     */
    public function getAllStations() {
        try {
            $all_stations = \App\Models\Station::get_all();
            return json_encode($all_stations);
        } catch (\Exception $e) {

        }
    }

    /**
     * @author Sumit Kohli <sumit.kohli12345@gmail.com>
     * Description - Display a listing of Searched routes.
     *
     * @return Response
     */
    public function search(Request $request) {
        try {
            $all_stations = \App\Models\Station::get_all();
            $all_stations = array_combine(array_column($all_stations, 'id'), $all_stations);
            $all_paths = \App\Models\StationDistance::get_all();

            $all_stations_paths = $this->prepareStationData($all_stations, $all_paths);
            $destination_routes = $this->getRoutes(
                    $all_stations_paths, $all_stations[$request->from]['station_name'], $all_stations[$request->destination]['station_name']
            );

            return json_encode($this->getPathResponse($all_stations, $destination_routes, $all_stations[$request->from]['station_name'], $all_stations[$request->destination]['station_name']));
        } catch (\Exception $e) {
            return json_encode(['status' => FALSE, 'response' => ['error' => 'Something went wrong']]);
        }
    }


    /*############################Private Functions################################*/
    private function getRoutes($all_stations_paths, $a, $b, $recur = 0, $i = 0) {

        $paths = [];

        foreach ($all_stations_paths as $key => $value) {
            
            if ($a == $key) {
                if (array_key_exists($b, $value)) {
                    if ($recur == 1) {
                        return [$b => $value[$b]];
                    }
                    $paths[$i] = [$b => $value[$b]];
                    $visited = $b;
                    $i++;
                }

                unset($all_stations_paths[$a]);


                foreach ($value as $key1 => $val1) {
                    if (!empty($visited) && $visited == $key1) {
                        continue;
                    }

                    $getPath = $this->getRoutes($all_stations_paths, $key1, $b, 1, $i);

                    if (!empty($getPath)) {                        
                        if ($this->is_multi_array($getPath)) {
                            foreach ($getPath as $path) {
                                $paths[$i][$key1] = $val1;                                
                                $paths[$i] = array_merge($paths[$i], $path);
                                $i++;
                            }
                        } else {
                            $paths[$i][$key1] = $val1;
                            $paths[$i] = array_merge($paths[$i], $getPath);
                            $i++;
                        }
                    }
                }
            }
        }
        return $paths;
    }
    
    private function getPathResponse($all_stations, $destination_routes, $a, $b) {

        if (empty($destination_routes)) {
            return ['status' => FALSE, 'response' => [
                'total_routes' => 0,
                'error' => 'Sorry, no routes available'
            ]];
        }

        $returnArr = [
            'from' => $a, 
            'destination' => $b,
            'total_routes' => count($destination_routes)
        ];

        foreach ($destination_routes as $key => $value) {
            $returnArr['paths'][$key] = ['distance' => array_sum($value)];
            $returnArr['paths'][$key]['nodes'][0] = ['station' => $a, 'distance' => '0'];

            foreach ($value as $station => $distance) {
                $returnArr['paths'][$key]['nodes'][] = compact('station', 'distance');
            }
        }

        usort($returnArr['paths'], function ($a, $b) {

            if ($a['distance'] == $b['distance']) {
                return count($a['nodes'] < $b['nodes']) ? -1 : 1;
            }
            return ($a['distance'] < $b['distance']) ? -1 : 1;
        });

        return ['status' => TRUE, 'response' => $returnArr];
    }

    private function prepareStationData($all_stations, $all_paths) {
        $returnArr = [];

        foreach ($all_paths as $path) {
            $returnArr[$all_stations[$path['source_station_id']]['station_name']][$all_stations[$path['destination_station_id']]['station_name']] = $path['distance'];
        }

        return $returnArr;
    }
    
    public  function is_multi_array($arr) {
        if (empty($arr))
            return FALSE;
        $rv = array_filter($arr, 'is_array');

        if (count($rv) > 0)
            return true;

        return false;
    }

}
