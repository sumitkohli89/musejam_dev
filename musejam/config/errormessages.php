<?php

return [
    'SYSTEM_MESSAGES' => [

        /* General Errors */
        'CURL_ERROR' => ['code' => '1', 'is_send' => FALSE, 'message' => 'Something went wrong'],
        'HOST_ERROR' => ['code' => '2', 'is_send' => TRUE, 'message' => 'Something went wrong. Please try again later'],
        'NO_KEYWORD_FOUND' => ['code' => '3', 'is_send' => TRUE, 'message' => 'Type "Hi" to login or "show" to view our services'],
        'UNAUTHORISED_ACCESS' => ['code' => '4', 'is_send' => TRUE, 'message' => 'Type "Hi" to login or "show" to view our services'],

        /* Facebook Reated Errors */
        'FACEBOOK_AUTH_TOKEN_EXPIRED' => ['code' => '100001', 'is_send' => FALSE, 'message' => 'Login session expired. Please try again after sometime.'],
        'FACEBOOK_PERSON_NOT_RECIEVING_MESSAGE' => ['code' => '100002', 'is_send' => FALSE, 'message' => 'Login session expired. Please try again after sometime.'],
        'FACEBOOK_REQUEST_LIMIT_REACHED' => ['code' => '100003', 'is_send' => FALSE, 'message' => 'Login session expired. Please try again after sometime.'],
        'FACEBOOK_INVALID_USER' => ['code' => '100004', 'is_send' => FALSE, 'message' => 'Login session expired. Please try again after sometime.'],
        'FACEBOOK_INVALID_TOKEN' => ['code' => '100005', 'is_send' => FALSE, 'message' => 'Login session expired. Please try again after sometime.'],

        /* Oxiface Reated Errors */
        'OXIFACE_USER_NOT_EXIST' => ['code' => '200001', 'is_send' => TRUE, 'message' => 'You are not registered with oxigen wallet'],
        'OXIFACE_INVALID_OTP' => ['code' => '200002', 'is_send' => TRUE, 'message' => 'You have entered a wrong otp'],
        'OXIFACE_INSUFFICIENT_BALANCE' => ['code' => '200003', 'is_send' => TRUE, 'message' => 'Insufficient Balance'],
        'OXIFACE_BENEFICIARY_LIMIT_EXCEEDED' => ['code' => '200004', 'is_send' => TRUE, 'message' => 'Beneficiary upper limit exceeded'],
        'OXIFACE_SAME_SOURCE_RECIEVER' => ['code' => '200005', 'is_send' => TRUE, 'message' => 'You cannot send money to yourself'],
    ],

    'SYSTEM_MESSAGES_REVERSE_MAPPING' => [
        '1' => 'CURL_ERROR',
        '2' => 'HOST_ERROR',
        '3' => 'NO_KEYWORD_FOUND',
        '4' => 'UNAUTHORISED_ACCESS',
        '100001' => 'FACEBOOK_AUTH_TOKEN_EXPIRED',
        '100002' => 'FACEBOOK_PERSON_NOT_RECIEVING_MESSAGE',
        '100003' => 'FACEBOOK_REQUEST_LIMIT_REACHED',
        '100004' => 'FACEBOOK_INVALID_USER',
        '100005' => 'FACEBOOK_INVALID_TOKEN',

        '200001' => 'OXIFACE_USER_NOT_EXIST',
        '200002' => 'OXIFACE_INVALID_OTP',
        '200003' => 'OXIFACE_INSUFFICIENT_BALANCE',
        '200004' => 'OXIFACE_BENEFICIARY_LIMIT_EXCEEDED',
        '200005' => 'OXIFACE_SAME_SOURCE_RECIEVER',
    ],

    'FACEBOOK_ERRORS' => [
        '2' => 'FACEBOOK_PERSON_NOT_RECIEVING_MESSAGE',
        '4' => 'FACEBOOK_REQUEST_LIMIT_REACHED',
        '100' => 'FACEBOOK_INVALID_USER',
        '190' => 'FACEBOOK_INVALID_TOKEN',
        '200' => 'FACEBOOK_PERSON_NOT_RECIEVING_MESSAGE',
        '551' => 'FACEBOOK_PERSON_NOT_RECIEVING_MESSAGE',
        '10301' => 'FACEBOOK_AUTH_TOKEN_EXPIRED',
    ],
    'OXIFACE_ERRORS' => [
        '16' => 'OXIFACE_USER_NOT_EXIST',
        '29' => 'OXIFACE_INVALID_OTP',
        '31' => 'OXIFACE_INSUFFICIENT_BALANCE',
        '47' => 'OXIFACE_BENEFICIARY_LIMIT_EXCEEDED',
        '220' => 'OXIFACE_SAME_SOURCE_RECIEVER',
    ]
    
];
