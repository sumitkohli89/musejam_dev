<?php

return [
    'SITE_NAME' => 'Oxigen',
    'LOGIN_TYPE' => 'OTP',//AUTH
    
    'OXIFACE' => [
        'API_URL' => 'https://qa-of.oxigenwallet.com/oxiface?enc=1&ab=1',
//        'API_URL' => 'https://dev-of.oxigenwallet.com/oxiface?debug=1&enc=1&ab=1',
        'REQUESTER' => '11',
        'INSTRUMENT' => '3'
    ],
    'FACEBOOK' => [
        'API_URL' => 'https://graph.facebook.com/v2.6/',
        'PAGE_TOKEN' => 'EAAWEPCbbVHIBAB9ldYPKGimgFxWKH5sSboOEqjEYTgSIXNLcwZBMpDs5609t3DDq0g2yajZCu75hIpwnbVemWNwCO5BWLrDULIw3DhZCNivmkx551ZAzuhMjHOjDZAcCUwDMVy41RcVUR5Iv5WH6Dp6OyIEvuKZB6P1sZCU5F2MOwZDZD',
        'DEFAULT_EVENTS' => [
            'login_success' => 'login_success',
            'logout_success' => 'logout_success',
            'text' => 'post_text'
        ],
    ],

    'DB_STATUS' => ['ACTIVE' => '1', 'IN_ACTIVE' => '2', 'DELETED' => '0', 'PENDING' => '3', 'CANCELLED' => '4'],
    
    'SITE_MESSAGES' => [
        'GREETING_TEXT' => 'Welcome to oxigen wallet. Kindly login to view our services',
        'WELCOME_BACK' => '%s, good to see you again',
        'USER_SAVE_SUCCESS' => 'User Saved Successfully',
        'DEFAULT_ERROR_MESSAGE' => 'Something went wrong. Please contact administrator',
        'UNAUTHORISED_ACCESS' => 'You are not login',
        'WALLET_BALANCE' => 'Your wallet balance is ₹%s :)',
        'P2P_SUCCESS' => '(Y) Hurray, Your transaction is successfull',
        'VAS_SUCCESS' => '(Y) Hurray, Your transaction is successfull',
        'CANCEL_TRANSACTION' => 'Your transaction is cancelled.',
        'SHOW_SERVICES' => 'You can type "show" to view our services.',
        'OTP_LOGIN' => 'Enter your 10 digit Oxigen Wallet\'s number',
        'ENTER_OTP' => 'Kindly enter one time password to authenticate',
        'LOGOUT_SUCCESS' => 'You have been logged out. You can login any time by typing "login" or "hi"',
        'LOGIN_SUCCESS' => 'Hi %s, good to see you. You can logout any time by typing "logout"',
        'ALL_TRANSACTIONS_CANCELLED' => 'Request Cancelled',
        'ALREADY_LOGGED_IN' => 'You are already logged in with wallet %s. Type "logout" if you want login with different number',
    ],
    
    'EXCEPTIONS' => [
        'FACEBOOK_NO_KEYWORD_FOUND' => ['message' => 'Type Hi to login or show to view services', 'handler' => TRUE, 'social' => 'Facebook']
    ],
    
    'VAS_OPERATORS' => [
        'ETOP' => [
            'operators' => ['AIRTFTT' => 'AIRTEL', 'VODA' => 'Vodafone', 'IDEA' => 'Idea', 'BSNL' => 'BSNL', 'AIRC' => 'Aircel'],
            'vas_type' => 'MOBILE'
        ]
    ],
    'EVENTS' => [
        'VAS_RECHARGE' => ['action' => 'vas'],
        'CANCEL_TRANSACTION' => ['action' => 'cancel_transaction']
    ]    
];
