-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 24, 2016 at 11:22 PM
-- Server version: 5.5.50-0ubuntu0.14.04.1
-- PHP Version: 5.6.20-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `musejam`
--

-- --------------------------------------------------------

--
-- Table structure for table `stations`
--

CREATE TABLE IF NOT EXISTS `stations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `station_name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `stations`
--

INSERT INTO `stations` (`id`, `station_name`) VALUES
(1, 'S'),
(2, 'T'),
(3, 'U'),
(4, 'V'),
(5, 'W'),
(6, 'X');

-- --------------------------------------------------------

--
-- Table structure for table `station_distance`
--

CREATE TABLE IF NOT EXISTS `station_distance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_station_id` int(11) NOT NULL,
  `destination_station_id` int(11) NOT NULL,
  `distance` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `station_distance`
--

INSERT INTO `station_distance` (`id`, `source_station_id`, `destination_station_id`, `distance`) VALUES
(1, 1, 2, 7),
(2, 1, 4, 2),
(3, 1, 6, 4),
(4, 2, 6, 1),
(5, 2, 4, 2),
(6, 3, 1, 2),
(7, 3, 2, 6),
(8, 6, 4, 1),
(9, 5, 3, 5),
(10, 5, 1, 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
